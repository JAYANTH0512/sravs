package Keerthana;

//import java.io.File;
//import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
/**
 * To create a class to implement the file creation/delete the methods or parameters
 * By using jsonObject 
 * @author jpalepally1
 */
public class FileCreation {
	//To create the main method
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		
		//To create a jsonObject
		JSONObject obj1 = new JSONObject();
		
	     obj1.put("name", "swapna"); 
	     obj1.put("ID", "111718039103");
	     obj1.put("cource", "mca");
	     
	     //To create another jsonObject
	     JSONObject obj2 = new JSONObject();
	     
	     obj2.put("name", "keerthana");
	     obj2.put("ID", "111718039130");
	     obj2.put("cource", "btech");
	     
	     //To create a jsonArray object
	     JSONArray array = new JSONArray();
	     array.add(obj1);
	     array.add(obj2);
	     
	     try {
	         FileWriter file = new FileWriter("array");
	         file.write(array.toJSONString());
	         file.close();
	      } catch (IOException e) {
	         
	         e.printStackTrace();
	      }   
	    
	   	      System.out.println("JSON file created: "+array);
	   	      
	   	      
	      //To create an object for deleting a file
//	      File myObj = new File("student.txt"); 
//		     if (myObj.delete()) { 
//		       System.out.println("Deleted the file: " + myObj.getPath());
//		     } else {
//		       System.out.println("Failed to delete the file.");
//		     } 

	      
}
}
